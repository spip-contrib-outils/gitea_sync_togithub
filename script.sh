#!/bin/bash

hash curl 2>/dev/null || { echo >&2 "curl is required. Stopped."; exit 1; }
hash git 2>/dev/null || { echo >&2 "git is required. Stopped."; exit 1; }
hash ssh-keygen 2>/dev/null || { echo >&2 "ssh-keygen is required. Stopped."; exit 1; }

if [[ ! -f script.config ]]; then
        echo >&2 "script.config is required. Stopped."; exit 1;
fi

# shellcheck source=script.config.sample
source script.config

function github_create_repo() {
        local project_name=$1
        local endpoint="/orgs/$GITHUB_ORGANIZATION/repos"
        curl \
                -H "Accept: application/vnd.github.v3+json" \
                -H "Authorization: token $GITHUB_TOKEN" \
                --data "{\"name\":\"${project_name%%.*}\",\"homepage\":\"$GITEA_URL/${project_name%%.*}\",\"has_issues\":\"false\",\"has_wiki\":\"false\",\"team_id\":$GITHUB_TEAMID}" \
                "$GITHUB_BASE$endpoint"
}

function github_deploy_key() {
        local project_name=$1
        local endpoint="/repos/$GITHUB_ORGANIZATION/$project_name/keys"
        local key

        key=$(cat "$SSHKEY_PATH/$project_name.pub")

        curl \
                -H "Accept: application/vnd.github.v3+json" \
                -H "Authorization: token $GITHUB_TOKEN" \
                --data "{\"key\":\"${key}\",\"readonly\":false,\"title\":\"Gitea - $project_name\"}" \
                "$GITHUB_BASE$endpoint"
}

function ssh_create_sshkey() {
        local project_name=$1
        if [[ ! -f "$SSHKEY_PATH/$project_name" ]]; then
                ssh-keygen -t rsa -f "$SSHKEY_PATH/$project_name" -q -N ""
        fi
}

function gitea_declare_key() {
        local project_path=$1
        local project_name=$2

        if [[ -f "$SSHKEY_PATH/$project_name" ]]; then
                git -C "$project_path" config core.sshCommand "ssh -i $SSHKEY_PATH/$project_name"
        fi
}

function gitea_configure_remote() {
        local project_path=$1
        local project_name=$2

        git -C "$project_path" remote add --mirror=push github "git@github.com:spip/${project_name}"
}


function gitea_push_github() {
        local project_path=$1

        git -C "$project_path" push github
}

for project_path in $(ls -d "$PWD/*"); do
        project_dir=$(basename "$project_path")
        project_name=${project_dir%%.*}

        github_create_repo "$project_name"
        gitea_configure_remote "$project_path" "$project_name"
        ssh_create_sshkey "$project_name"
        github_deploy_key "$project_name"
        gitea_declare_key "$project_path" "$project_name"
        gitea_push_github "$project_path"
done