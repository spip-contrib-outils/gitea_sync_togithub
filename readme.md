# Synchronize local repo to github repo

Configure script.config file and set all variable as :

    GITHUB_TOKEN="[any string token provide by github]" 
    GITHUB_ORGANIZATION="[name organization]"
    GITHUB_BASE="https://api.github.com"
    GITHUB_TEAMID=[numeric value]

    GITEA_URL="[url root repositoies as https://git.spip.net/spip]"
    SSHKEY_PATH="[ssh key directory]"

Goto root repositories directory as **/var/git/repositories/spip**

execute **script.sh**